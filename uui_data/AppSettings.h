#import <Foundation/Foundation.h>

/**
 Facade onto app settings stored in NSUserDefaults.
 */
@interface AppSettings : NSObject

+ (void)registerDefaultsFromSettingsBundle;

+ (NSString*) uu_id;
+ (NSString*) server_url;
+ (NSString*) destination;

@end

