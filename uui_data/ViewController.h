//
//  ViewController.h
//
//  Created by Richard Morgan on 08/11/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVKit/AVKit.h>
#import <AVFoundation/AVFoundation.h>
#import <ACBClientSDK/ACBUC.h>


@interface ViewController : UIViewController <ACBClientCallDelegate, ACBUCDelegate>


@property (weak, nonatomic) ACBUC *acbuc;

/// Config
+ (ACBUC *) getACBUC;


@end

