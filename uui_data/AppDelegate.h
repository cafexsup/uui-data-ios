//
//  AppDelegate.h
//
//  Created by Richard Morgan on 08/11/2017.
//  Copyright © 2017 cafex. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

