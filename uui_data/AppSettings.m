#import "AppSettings.h"

@implementation AppSettings

+ (void)registerDefaultsFromSettingsBundle {
    // this function writes default settings as settings
    NSUserDefaults * standardUserDefaults = [NSUserDefaults standardUserDefaults];
    NSString * uu_id = [standardUserDefaults objectForKey:@"uu_id"];
    if (!uu_id) {
        NSString *settingsBundle = [[NSBundle mainBundle] pathForResource:@"Settings" ofType:@"bundle"];
        if(!settingsBundle) {
            NSLog(@"Could not find Settings.bundle");
            return;
        }
        
        NSDictionary *settings = [NSDictionary dictionaryWithContentsOfFile:[settingsBundle stringByAppendingPathComponent:@"Root.plist"]];
        NSArray *preferences = [settings objectForKey:@"PreferenceSpecifiers"];
        
        NSMutableDictionary *defaultsToRegister = [[NSMutableDictionary alloc] initWithCapacity:[preferences count]];
        for(NSDictionary *prefSpecification in preferences) {
            NSString *key = [prefSpecification objectForKey:@"Key"];
            if(key) {
                [defaultsToRegister setObject:[prefSpecification objectForKey:@"DefaultValue"] forKey:key];
                NSLog(@"writing as default %@ to the key %@",[prefSpecification objectForKey:@"DefaultValue"],key);
            }
        }
        
        [[NSUserDefaults standardUserDefaults] registerDefaults:defaultsToRegister];
    }
}

+ (NSString*) uu_id {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"uu_id"];
}

+ (NSString*) server_url {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"server_url"];
}

+ (NSString*) destination {
    return [[NSUserDefaults standardUserDefaults] objectForKey:@"destination"];
}


@end

