//
//  ViewController.m
//
//  Created by Richard Morgan on 08/11/2017.
//  Copyright © 2017 cafex. All rights reserved.
//
#import "AppSettings.h"
#import "ViewController.h"
#import <ACBClientSDK/ACBUC.h>
#import <UIKit/UIKit.h>

@interface ViewController ()

@property (strong, nonatomic) IBOutlet UIButton *call;
@property (strong, nonatomic) IBOutlet UITextView *textuuid;

@end

@implementation ViewController

static ACBUC *acbuc;

+ (ACBUC *) getACBUC
{
    return acbuc;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    NSString *uu_id = [AppSettings uu_id];
    NSLog(@"UUID: %@", uu_id);
    
    NSString *uuid_string = [NSString stringWithFormat:@"UUID HEADER: %@", uu_id];
    NSLog(@"UUID: %@", uu_id);
    
    _textuuid.text = [_textuuid.text stringByAppendingString:uuid_string];
    
}

- (IBAction)end:(id)sender {
    
    dispatch_async(dispatch_get_main_queue(), ^{
        /* Your UI code */
        
        self.acbuc = [ViewController getACBUC];
        
        [self.acbuc stopSession];
        
         NSLog(@"User hungup");
        
    });
    
}

- (IBAction)call_btn:(id)sender {
    
    NSString *uu_id = [AppSettings uu_id];
    NSLog(@"UUID: %@", uu_id);
    
    NSString *server_url = [AppSettings server_url];
    
    // get the session id...
    //NSString *urlString = [NSString stringWithFormat:@"http://192.168.0.84/cgi-bin/hello.cgi?uuid=%@", uu_id];
    
    NSString *urlString = [NSString stringWithFormat:@"%1$@?uuid=%2$@", server_url, uu_id];
    NSLog(@"urlString: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:urlString];
    NSString *sessionID = [NSString stringWithContentsOfURL:url encoding:NSASCIIStringEncoding
                                                      error:nil];
     NSLog(@"::sessionID: %@", sessionID);
    
// now init the with the session id
    
    NSDictionary *jsonDic = [NSJSONSerialization JSONObjectWithData:[sessionID dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
    NSString *sessionid = [jsonDic objectForKey:@"sessionid"];

    NSLog(@"::sessionid: %@", sessionid);

    acbuc = [ACBUC ucWithConfiguration:sessionid delegate:self];
    
     [acbuc setNetworkReachable:TRUE];
     [acbuc acceptAnyCertificate:TRUE];
     [acbuc setUseCookies:TRUE];
     [acbuc startSession];

}

-(void)ucDidStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidStartSession");
    
    
    // assign acbuc object and init local video stream
    self.acbuc = [ViewController getACBUC];

    [ACBClientPhone requestMicrophoneAndCameraPermission:true video:false];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        /* Your UI code */
        
        NSString *destination = [AppSettings destination];
        
        ACBClientCall *call = [self.acbuc.phone
                               createCallToAddress:destination
                               audio:ACBMediaDirectionSendAndReceive
                               video:ACBMediaDirectionNone
                               delegate:self];
        
        
    });
    
    
}

-(void)ucDidFailToStartSession:(ACBUC *)uc
{
    NSLog(@"::ucDidFailToStartSession");
    
    NSLog(@" uc %@", uc);
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
